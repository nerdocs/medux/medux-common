#  MedUX - Open Source Electronical Medical Record
#  Copyright (c) 2022  Christian González
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
import warnings

from django.conf import settings
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError, ImproperlyConfigured
from django.db import models
from django.utils.translation import gettext_lazy as _

from medux.common.models import Tenant
from medux.preferences.definitions import Scope, KeyType, ScopeIcons
from medux.preferences.registry import PreferencesRegistry


def validate_lower(value):
    if not value == value.lower():
        raise ValidationError("%(value)s must be lowercase.", params={"value": value})


class BasePreference(models.Model):
    """Basic model which holds namespace and key of a setting.
    ScopedPreference use this model as FK, to prevent redundant storage of preferences.
    """

    class Meta:
        verbose_name = verbose_name_plural = _("Preferences")
        ordering = ["namespace", "key"]

    namespace = models.CharField(max_length=25, validators=[validate_lower])
    key = models.CharField(max_length=255, validators=[validate_lower])

    def save(self, **kwargs):
        self.clean()
        super().save(**kwargs)

    def clean(self):
        """Make sure namespace and key are lowercase."""
        self.namespace = str(self.namespace).lower()
        self.key = str(self.key).lower()

    @classmethod
    def namespaces(cls) -> list[str]:
        """:return: a list of available namespaces."""
        # FIXME this is highly insufficient. distinct() would be better,
        #       but not available on SQLite/dev
        result = set()
        for s in cls.objects.order_by("namespace").values_list("namespace", flat=True):
            result.add(s)
        return list(result)

    @classmethod
    def keys(cls) -> list[tuple[str, str]]:
        """:return: a tuple[namespace,key] of all currently available keys."""

        return list(set((item.namespace, item.key) for item in cls.objects.all()))

    @property
    def icon(self):
        """:return: the preferences' icon name."""
        return PreferencesRegistry.icon(self.namespace, self.key)

    @property
    def key_type(self) -> KeyType:
        """:return: the setting's key type.

        This is a convenience method and gets it from the PreferencesRegistry."""
        return PreferencesRegistry.key_type(self.namespace, self.key)

    @property
    def help_text(self) -> str:
        """:return: the setting's help_text.
        This is a convenience method and gets it from the PreferencesRegistry."""
        return PreferencesRegistry.help_text(self.namespace, self.key)

    def __str__(self):
        return f"{self.namespace}.{self.key}"


class ScopedPreferenceManager(models.Manager):
    """A manager that allows easily creating ScopedPreferences with their
    BasePreference namespace/key attributes directly:

    ScopedPreference.objects.create(namespace="foo", key="bar", scope=...)

    The manager tries to find a Basepreference with that namespace/key and links to it.
    If none found, it is created.
    """

    def create(self, **kwargs):
        if "namespace" in kwargs and "key" in kwargs:
            if "base" in kwargs:
                # base AND namespace/key given...
                warnings.warn(
                    message="'base' and 'namespace'/'key' for "
                    "ScopedPreference creation given. "
                    "Ignoring 'namespace'/'key', just taking 'base'."
                )
                del kwargs["namespace"]
                del kwargs["key"]
            else:
                namespace = kwargs.pop("namespace")
                key = kwargs.pop("key")
                try:
                    kwargs["base"] = BasePreference.objects.get(
                        namespace=namespace, key=key
                    )
                except BasePreference.DoesNotExist:
                    kwargs["base"] = BasePreference.objects.create(
                        namespace=namespace, key=key
                    )

        return super().create(**kwargs)


class ScopedPreference(models.Model):
    """Model class for all scoped MedUX preferences.

    Preferences are generally saved as strings, but are interpreted at retrieval
    and cast into their correct types. ScopedPreference knows about
    ``str``, ``int``, ``bool``

    You can easily access preferences using
        ScopedPreference.get(namespace, key, scope, ...)
    """

    class Meta:
        verbose_name = verbose_name_plural = _("Scoped preferences")
        ordering = ["base__namespace", "base__key", "scope"]
        unique_together = [
            ["base", "scope", "tenant"],
            ["base", "scope", "group"],
            # TODO: ["base", "scope", "device"],
            ["base", "scope", "user"],
        ]
        permissions = [
            (
                "change_own_user_scopedpreference",
                _("Can change own user's preferences"),
            ),
            (
                "change_own_tenant_scopedpreference",
                _("Can change own tenant's preferences"),
            ),
            (
                "change_own_group_scopedpreference",
                _("Can change own groups' preferences"),
            ),
            ("change_device_scopedpreference", _("Can change a device's preferences")),
        ]

    objects = ScopedPreferenceManager()

    base = models.ForeignKey(
        BasePreference, on_delete=models.CASCADE, related_name="scopedpreferences"
    )
    """The FK to the basic preferences fields like namespace, key."""

    tenant = models.ForeignKey(
        Tenant,
        verbose_name=_("Tenant"),
        on_delete=models.CASCADE,
        default=None,
        null=True,
        blank=True,
    )
    group = models.ForeignKey(
        Group, on_delete=models.CASCADE, default=None, null=True, blank=True
    )
    # device = models.ForeignKey(
    #     "Device", on_delete=models.CASCADE, default=None, null=True, blank=True
    # )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        default=None,
        null=True,
        blank=True,
    )

    scope = models.IntegerField(choices=Scope.choices)
    """The scope where this setting is valid."""

    value = models.CharField(max_length=255, null=True)
    """The value of the setting. NULL means the setting using this scope
    was deleted, and the next matching scope is used."""

    @property
    def namespace(self) -> str:
        """:return: The preferences' namespace"""
        return self.base.namespace

    @property
    def key(self) -> str:
        """:return: The preferences' key"""
        return self.base.key

    @property
    def icon(self) -> str:
        """:return: the preferences' icon name."""
        return self.base.icon

    @property
    def key_type(self) -> KeyType:
        """:return: the preference's key type.

        This is a convenience method and gets it from the base preference."""
        return self.base.key_type

    @property
    def help_text(self) -> str:
        return self.base.help_text

    def clean(self):
        """Check if object FK is present if scope demands it."""

        # Make value a str
        if self.value is None:
            self.value = ""
        self.value = self.value.strip()
        # Scopes with their correct object FK
        if self.scope == Scope.USER:
            if not self.user:
                raise ValidationError(_("If scope is 'user', a user must be provided"))
        elif self.scope == Scope.DEVICE:
            if not self.device:
                raise ValidationError(
                    _("If scope is 'device', a device must be provided")
                )
        elif self.scope == Scope.GROUP:
            if not self.group:
                raise ValidationError(
                    _("If scope is 'group', a group must be provided")
                )
        elif self.scope == Scope.TENANT:
            if not self.tenant:
                raise ValidationError(
                    _("If scope is 'tenant', a tenant must be provided")
                )
        # TODO check if user/device/group is filled in if scope is one of them

    @classmethod
    def keys(cls) -> list[tuple[str, str]]:
        """:return: a Tuple[namespace,key] of all currently available keys."""

        return BasePreference.keys()

    @classmethod
    def namespaces(cls) -> list[str]:
        """:return: a list of available namespaces."""
        return BasePreference.namespaces()

    def __str__(self) -> str:
        # add user, tenant, group
        fk = ""
        if self.scope == Scope.USER:
            fk = f": '{self.user}'"
        elif self.scope == Scope.GROUP:
            fk = f": '{self.group}'"
        elif self.scope == Scope.TENANT:
            fk = f": '{self.tenant}'"
        a = ".".join([self.base.namespace, self.base.key])
        return f"{a} [{Scope(self.scope).name}{fk}]: {self.value}"

    @classmethod
    def assure_exists(cls, namespace, key, scope) -> None:
        """Raises KeyError if given preferences are not registered."""
        if not PreferencesRegistry.exists(namespace, key, scope):
            raise KeyError(
                f"Preference {namespace}.{key}/{scope.name} is not registered."
            )

    @classmethod
    def set(
        cls,
        namespace: str,
        key: str,
        scope: Scope,
        value: str | int | bool,
        user=None,
        tenant=None,
        device=None,
        group: Group = None,
    ) -> "ScopedPreference":
        """Convenience method to set a preferences key.

        Raises a TypeError if value is of incorrect KeyType
        """

        ScopedPreference.assure_exists(namespace, key, scope)

        # key_type is not used directly, as the value in the DB always is casted to a
        # str. Retrieving later is cast by the type saved in the PreferencesRegistry.

        key_type = PreferencesRegistry.key_type(namespace, key)
        if key_type == KeyType.INTEGER:
            # try to cast value into an integer. If not possible,
            # an Exception is raised.
            int(value)
        elif key_type == KeyType.BOOLEAN:
            if value not in (True, False):
                raise TypeError("value must be a Boolean.")

        filter = {
            "scope": scope,
        }
        # set correct scope
        if scope == Scope.USER:
            if user is None:
                raise AttributeError(
                    "When scope==USER, a user object must be provided."
                )
            filter["user"] = user

        elif scope == Scope.DEVICE:
            if device is None:
                raise AttributeError(
                    "When scope==DEVICE, a device object must be provided."
                )
            filter["device"] = device

        elif scope == Scope.GROUP:
            if group is None:
                raise AttributeError(
                    "When scope==GROUP, a group object must be provided."
                )
            filter["group"] = group

        elif scope == Scope.TENANT:
            if tenant is None:
                raise AttributeError(
                    "When scope==TENANT, a tenant object must be provided."
                )
            filter["tenant"] = tenant

        # TODO: maybe do some casting / checks here?

        # if base setting does not exist, create it
        base, base_created = BasePreference.objects.get_or_create(
            namespace=namespace, key=key
        )
        if base_created:
            base.save()
        filter["base"] = base

        # get_or_create can't be used, as old / new value could differ,
        # so get() must not include it.
        try:
            item = ScopedPreference.objects.get(**filter)
        except ScopedPreference.DoesNotExist:
            filter["value"] = value
            item = ScopedPreference.objects.create(**filter)

        item.value = str(value)
        return item.save()

    @classmethod
    def get(
        cls,
        namespace: str,
        key: str,
        scope: Scope,
        full_object: bool = False,
        user=None,
        group: Group | None = None,
        device=None,  # TODO add device support
        tenant: Tenant | None = None,
    ) -> int | str | bool | models.Model | None:
        """Convenience method to retrieve a preferences key.

        Attributes:
            namespace: the namespace this key is saved under. Usually the app's name.
            key: the key to be retrieved
            scope: the scope that key is valid for. If scope is None,
                and more than one keys are saved under that scope, the key
                with the highest priority is taken:
                USER > DEVICE > GROUP > TENANT > VENDOR
                If scope is USER, DEVICE, GROUP, TENANT, then an additional argument
                must be provided with the specific object:
                `get("common", "country", Scope.TENANT, tenant=user.tenant)`
            full_object: if True, return the complete model instance instead of only
                its value
            user: if scope is USER, you have to provide a User object that key/scope
                is valid for.
            group: if scope is GROUP, you have to provide a SettingsGroup object that
                key/scope is valid for.
            device: if scope is DEVICE, you have to provide a Device object that
                key/scope is valid for.
            tenant: if scope is TENANT, you have to provide a Tenant object that
                key/scope is valid for.

        Returns:
            A namespaced preferences value, according to given scope
            and, if applicable, the related object like user, group etc.
            If no ScopedPreference object is found, None is returned.
            If pointing to an unregistered namespace/key, raise a KeyError.
        """

        ScopedPreference.assure_exists(namespace, key, scope)
        key_type = PreferencesRegistry.key_type(namespace, key)

        filters = {
            "base__namespace": namespace,
            "base__key": key,
        }
        if scope:
            filters["scope"] = scope
            if scope == Scope.USER:
                filters["user"] = user
            elif scope == Scope.GROUP:
                filters["group"] = group
            elif scope == Scope.DEVICE:
                filters["device"] = device
            elif scope == Scope.TENANT:
                filters["tenant"] = tenant

        objects = ScopedPreference.objects.filter(**filters)
        if len(objects) == 0:
            # if this setting does not exist (yet), return None
            return None

        if len(objects) == 1:
            obj = objects.first()
            if full_object:
                return obj
            value = obj.value  # type: str
        else:
            # more than one scopes under that key and scope
            ids = ",".join(i.id for i in objects)
            raise KeyError(
                f"There are multiple preferences keys "
                f"under {namespace}.{key}[{scope}]: ids {ids}"
            )
        # filter out int and boolean values and return them instead.
        if key_type == KeyType.INTEGER:
            return int(value)
        if key_type == KeyType.BOOLEAN:
            if value.lower() == "false":
                return False
            elif value.lower() == "true":
                return True
            else:
                raise ValueError(f"DB value '{value}' cannot be casted into boolean!")
        if key_type in (KeyType.STRING, KeyType.TEXT):
            return str(value)

        # should never happen...
        raise KeyError(f"Unknown preferences type: {key_type}")

    @classmethod
    def get_effective(cls, namespace: str, key: str, scope: Scope, **kwargs):
        """Return "effective" preference for given object
        (USER, DEVICE, (GROUP,) TENANT).

        Attributes:
            namespace: the preference namespace to find
            key: the preference key to find
            scope: the preference scope to find
            kwargs: the object (as key/value) the preference is related to,
                in the form `tenant=MyTenant`

        If object preference is not found, walk up in the Scope tree until a preference
        is found. If given object preference is not found, take this map to escalate:
        USER   -> GROUP
        (GROUP  -> TENANT) TODO: not implemented yet
        DEVICE -> TENANT
        TENANT -> VENDOR
        """

        if kwargs:
            # get first key
            current_object_name = next(iter(kwargs))
            current_object = kwargs[current_object_name]
        else:
            current_object_name = "vendor"
            current_object = None

        while current_object_name is not None:
            if PreferencesRegistry.exists(namespace, key, scope):
                filter = {current_object_name: current_object} if current_object else {}
                value = ScopedPreference.get(
                    namespace=namespace, key=key, scope=scope, **filter
                )
                if value is not None:
                    return value
                else:
                    # we have no value using the current scope, so walk up the tree
                    match current_object_name:
                        case "user":
                            # get users groups
                            current_object_name = "tenant"
                            # FIXME: Django does not support group.tenant
                            # scope=Scope.GROUP
                            # current_object = current_object.groups.all()
                            scope = Scope.TENANT
                            current_object = current_object.tenant
                            if not current_object:
                                # user has no tenant... directly go to vendor
                                current_object_name = "vendor"
                                scope = Scope.VENDOR
                                current_object = None
                        # case "group":
                        #     current_object_name = "tenant"
                        #     current_object = current_object.tenant
                        case "device":
                            current_object_name = "tenant"
                            scope = Scope.TENANT
                            current_object = current_object.tenant
                        case "tenant":
                            current_object_name = "vendor"
                            scope = Scope.VENDOR
                            current_object = None
                        case "vendor":
                            current_object_name = None
                            current_object = None

        # this should not happen. Every preference must have at least a VENDOR value
        raise ImproperlyConfigured(
            "Could not get preference for {namespace}.{key}.".format(
                namespace=namespace, key=key
            )
        )

    def get_scope_icon(self) -> str:
        """:return: the icon name of the preferences' scope."""
        if not self.scope:
            return ""
        scope = Scope(self.scope)
        return ScopeIcons[scope.name].value if scope else ""

    def get_related_object(self):
        match self.scope:
            case Scope.TENANT:
                return self.tenant
            case Scope.USER:
                return self.user
            case Scope.DEVICE:
                return self.device
            case Scope.GROUP:
                return self.group
            case Scope.VENDOR:
                return None
                # TODO: maybe return Vendor name here?
