#  MedUX - Open Source Electronical Medical Record
#  Copyright (c) 2022  Christian González
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
import sys
from pathlib import Path

from django.conf import settings
from django.db import DatabaseError
from django.utils.translation import gettext_lazy as _
from django.core.checks import Error, register, Tags

from medux.common.api import MeduxPluginAppConfig
from medux.common import __version__
from .registry import PreferencesRegistry

logger = logging.getLogger(__file__)


class PreferencesConfig(MeduxPluginAppConfig):
    name = "medux.preferences"
    verbose_name = _("Preferences")
    default = True  # FIXME: Remove when django bug is fixed
    groups_permissions = {
        "Users": {"preferences.ScopedPreference": ["view", "change_own_user"]},
        "Tenant admins": {
            "preferences.ScopedPreference": ["change_own_tenant"],
            "common.Tenant": ["change"],
        },
    }

    class PluginMeta:
        """This configuration is the introspection data for plugins."""

        # the plugin machine "name" is taken from the AppConfig, so no name here
        verbose_name = _("Preferences")
        author = "Christian González"
        author_email = "office@nerdocs.at"
        vendor = "Nerdocs"
        description = _(
            "MedUX preferences tools, which are used for MedUX Online and MedUX"
        )
        category = _("Base")
        visible = True
        version = __version__

    # compatibility = "medux.core>=2.3.0"


@register(Tags.database)
def check_if_vendor_preferences_have_defaults(app_configs, **kwargs):
    from medux.preferences.models import ScopedPreference
    from medux.preferences.tools import get_vendor_preference
    from medux.common.models import Tenant

    errors = []

    if kwargs["databases"]:
        # we have to wrap that into a try/except block because of the possibility of
        # running that check BEFORE the first migration was run. In that case the
        # check would fail, as not database is ready yet.
        try:
            # if there are no tenants, we can skip the check
            if Tenant.objects.count() == 0:
                return []

            for namespace, key, scope, key_type in PreferencesRegistry.all():
                try:
                    get_vendor_preference(namespace, key)
                except ScopedPreference.DoesNotExist:
                    errors.append(
                        Error(
                            f"Preference '{namespace}.{key}' "
                            "has no VENDOR preference defined.",
                            hint=f"Define a '{key}' key in preferences.toml under the "
                            f"'[{namespace}]' section",
                            obj=PreferencesRegistry,
                            # TODO: create a list of medux errors in  a central place
                            id="medux.preferences.E001",
                        )
                    )
        except DatabaseError as e:
            sys.stdout.write(
                f"Skipping vendor preferences check: database is not ready at this "
                f"stage.\n"
            )
    return errors


@register(Tags.files)
def check_preferences_file_exists(app_configs, **kwargs):
    errors = []
    pref_file = Path(settings.MEDUX["PREFERENCES_FILE"])
    if not pref_file.exists():
        errors.append(
            Error(
                f"Preferences file not found at {pref_file}",
                hint="Create a preferences.toml file (e.g. using 'manage.py "
                "generate_preferences' and specify its path in your .env under "
                "MEDUX['PREFERENCES_FILE'].",
                id="medux.preferences.E002",
            )
        )
    return errors
