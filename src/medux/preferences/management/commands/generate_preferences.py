import logging

from django.core.checks import Tags
from django.core.management import BaseCommand


logger = logging.getLogger(__file__)


class Command(BaseCommand):
    """Generate a blank preferences.toml file with all known scoped preferences"""

    help = (
        "DEV COMMAND: Generate a blank preferences.toml file with all known "
        "scoped preferences."
    )
    requires_system_checks = [Tags.compatibility, Tags.models]

    def handle(self, *args, **options):
        from medux.preferences.registry import PreferencesRegistry

        self.stdout.write(
            "\n# You can copy this lines into your preferences.toml file:"
        )
        for namespace, dct in PreferencesRegistry.all_dct().items():
            self.stdout.write(f"[{namespace}]")
            for key, scopes in dct.items():
                key_type = PreferencesRegistry.key_type(namespace, key)
                scopes_str = ""
                for scope, value in scopes.items():
                    if scopes_str:
                        scopes_str += ", "
                    scopes_str += scope.name
                self.stdout.write(
                    f"# {PreferencesRegistry.help_text(namespace, key)}, [{key_type}], "
                    f"Scopes: {scopes_str} "
                )
                self.stdout.write(f"{key}=''")
