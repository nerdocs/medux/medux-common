#  MedUX - Open Source Electronical Medical Record
#  Copyright (c) 2022  Christian González
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
import warnings

from django.db import models

from medux.preferences.definitions import KeyType
from medux.preferences.models import ScopedPreference
from medux.preferences.tools import get_effective_preference


class CachedPreferences:
    """Creates a request specific set of preferences, usable for templates.

    CachedPreferences allows easy access to the preferences for a specific context,
    given a request:

    CachedPreferences(request).my_namespace.my_setting returns the specific setting.
    It respects the current user, groups, device, and tenant

    # It creates a dict with nested preferences:
    # <namespace>
    # +-<key-1>
    #   +-user
    #   | +- value: 1
    #   | +- user: 23
    #   +-device
    #     +- value: 2
    #     +- device: 23543
    # ...
    """

    from django.http import HttpRequest

    class Namespace:
        """Transparently represents the first part (=namespace, before the dot) of a
        key, when accessed as CachedPreferences' attribute, e.g.
        ```python
        preferences = CachedPreferences(request)
        preferences.prescriptions.medication_message
        ```
        """

        from django.http import HttpRequest

        def __init__(self, request: HttpRequest, namespace: str):
            self.namespace = namespace
            self.request = request

        def __repr__(self):
            return f"<Namespace '{self.namespace}'>"

        def __getattr__(self, key: str) -> str | None:
            """get a list of preferences items with that key, ordered by scope:

            The scope order is: USER > DEVICE > GROUP > TENANT > VENDOR
            If a key is found during the traversal, its value is returned, else the
            next scope is looked up. If no key is found, None is returned.
            """

            preference = get_effective_preference(
                namespace=self.namespace,
                key=key,
                request=self.request,
            )
            # special case boolean.False, or None: return "" for  template variables
            if (
                preference is None
                or preference.key_type == KeyType.BOOLEAN
                and preference.value.lower() == "false"
            ):
                return ""
                # FIXME: this should be done better, distinguish between template and
                #       view code

            return preference.value

    def __init__(self, request: HttpRequest):
        self.request = request

    def __getattr__(self, namespace):
        """Helper to retrieve preferences key (all scopes) in a pythonic way:

        CachedPreferences.<namespace>.<key>"""

        return self.Namespace(self.request, namespace)

    @classmethod
    def get(cls, **kwargs) -> int | str | bool | models.Model | None:
        """Convenience method to retrieve a preferences key."""
        warnings.warn(
            "CachedPreferences.get() is deprecated. Use ScopedSettings.get() "
            "instead.",
            DeprecationWarning,
        )
        return ScopedPreference.get(**kwargs)

    @classmethod
    def set(cls, **kwargs):
        """Convenience method to set a preferences key."""
        warnings.warn(
            "CachedPreferences.set() is deprecated. Use ScopedSettings.set() "
            "instead.",
            DeprecationWarning,
        )
        return ScopedPreference.set(**kwargs)


# @receiver(request_finished)
# def on_request_finished(sender, **kwargs):
#     preferences.invalidate()
