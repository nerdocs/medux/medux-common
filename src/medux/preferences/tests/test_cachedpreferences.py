import pytest
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from medux.common.models import Tenant
from medux.preferences.definitions import Scope
from medux.preferences.models import ScopedPreference
from medux.preferences.registry import PreferencesRegistry
from medux.preferences.cached_preferences import CachedPreferences

User = get_user_model()


@pytest.mark.django_db
def test_cachedpreferences_namespace_type(request):
    namespace = CachedPreferences(request).mynamespace
    assert type(namespace) == CachedPreferences.Namespace


@pytest.mark.django_db
def test_cachedpreferences_key_none(request):
    key = CachedPreferences(request).test.non_existing_key
    assert key == ""


@pytest.mark.django_db
def test_cachedpreferences_existing_key_vendor(request):
    PreferencesRegistry.register("space", "my_key")
    ScopedPreference.set("space", "my_key", Scope.VENDOR, 42)
    assert CachedPreferences(request).space.my_key == "42"


@pytest.mark.django_db
def test_cachedpreferences_existing_key_tenant(request, user_a):
    PreferencesRegistry.register("space", "my_key", [Scope.TENANT])
    tenant = Tenant.objects.create(
        first_name="Demo", last_name="User", sex="f", address="Foo street"
    )

    ScopedPreference.set("space", "my_key", Scope.TENANT, 42, tenant=tenant)
    request.site = Site.objects.get(pk=1)
    user_a.tenant = tenant
    user_a.save()
    request.user = user_a
    space = CachedPreferences(request).space
    key = space.my_key
    assert key == "42"


@pytest.mark.django_db
def test_cachedpreferences_existing_key_with_correct_user(request, user_a):
    PreferencesRegistry.register("space", "my_key", [Scope.USER])
    ScopedPreference.set("space", "my_key", Scope.USER, 42, user=user_a)
    request.user = user_a
    assert CachedPreferences(request).space.my_key == "42"


@pytest.mark.django_db
def test_cachedpreferences_existing_key_with_wrong_user(request):
    user1 = User.objects.create_user(username="user1", password="user1")
    user2 = User.objects.create_user(username="user2", password="user2")

    ScopedPreference.set("space", "my_key", Scope.USER, 42, user=user1)
    request.user = user2
    assert CachedPreferences(request).space != ""

    key = CachedPreferences(request).space.my_key
    assert key == ""


@pytest.mark.django_db
def test_cachedpreferences_existing_key_user_fallback_vendor(request):
    user1 = get_user_model().objects.create(username="user1", password="user1")
    user2 = get_user_model().objects.create(username="user2", password="user2")
    PreferencesRegistry.register("space", "my_key", [Scope.USER])

    ScopedPreference.set("space", "my_key", Scope.VENDOR, 41)
    ScopedPreference.set("space", "my_key", Scope.USER, 42, user=user1)
    request.user = user2
    assert CachedPreferences(request).space.my_key == "41"
