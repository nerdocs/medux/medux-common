import pytest
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

from medux.preferences.definitions import Scope, KeyType
from medux.preferences.models import ScopedPreference
from medux.preferences.registry import PreferencesRegistry

User = get_user_model()


@pytest.fixture
def dummy_group():
    group = Group.objects.create(name="dummy")
    return group


@pytest.mark.django_db
def test_preferences_get_nonexisting():
    """test if non-existing setting returns None"""
    PreferencesRegistry.register("test", "non_existing", [Scope.VENDOR])
    result = ScopedPreference.get("test", "non_existing", Scope.VENDOR)
    assert result is None


@pytest.mark.django_db
def test_preferences_integer(user_admin, user_nobody):
    """test if an int preferences returns actually an int"""
    PreferencesRegistry.register(
        "test", "int_preferences", [Scope.USER], key_type=KeyType.INTEGER
    )
    ScopedPreference.set(
        "test",
        "int_preferences",
        Scope.USER,
        42,
        user=user_admin,
    )

    result = ScopedPreference.get(
        "test", "int_preferences", Scope.USER, user=user_admin
    )
    assert result == 42
    assert isinstance(int, result)

    result = ScopedPreference.get(
        "test", "int_preferences", Scope.USER, user=user_nobody
    )
    assert result is None


@pytest.mark.django_db
def test_spreferences_integer_casting(admin_user):
    """test if an int preferences returns actually an int"""
    PreferencesRegistry.register(
        "test", "cast_int_preferences", [Scope.USER], key_type=KeyType.INTEGER
    )
    ScopedPreference.set(
        "test", "cast_int_preferences", Scope.USER, "42", user=admin_user
    )

    assert (
        ScopedPreference.get(
            "test", "cast_int_preferences", Scope.USER, user=admin_user
        )
        == 42
    )


@pytest.mark.django_db
def test_preferences_str(admin_user):
    """test if a str preferences actually returns a str"""
    PreferencesRegistry.register(
        "test", "str_preferences", [Scope.USER], key_type=KeyType.STRING
    )
    ScopedPreference.set("test", "str_preferences", Scope.USER, "42a", user=admin_user)

    assert (
        ScopedPreference.get("test", "str_preferences", Scope.USER, user=admin_user)
        == "42a"
    )


@pytest.mark.django_db
def test_preferences_bool_true(admin_user):
    """test if a bool:True preferences actually returns bool:True"""
    PreferencesRegistry.register(
        "test", "true_preferences", [Scope.USER], key_type=KeyType.BOOLEAN
    )
    ScopedPreference.set("test", "true_preferences", Scope.USER, True, user=admin_user)
    result = ScopedPreference.get(
        "test", "true_preferences", Scope.USER, user=admin_user
    )
    assert result is True


@pytest.mark.django_db
def test_preferences_bool_false(admin_user):
    """test if a bool:False preferences actually returns bool:False"""
    PreferencesRegistry.register(
        "test", "false_preferences", [Scope.USER], key_type=KeyType.BOOLEAN
    )
    ScopedPreference.set(
        "test", "false_preferences", Scope.USER, False, user=admin_user
    )
    result = ScopedPreference.get(
        "test", "false_preferences", Scope.USER, user=admin_user
    )
    assert result is False


@pytest.mark.django_db
def test_spreferences_set_get_USER(admin_user, user_nobody):
    """test if a USER scoped setting returns the correct user and key"""
    PreferencesRegistry.register(
        "test", "user_preferences", [Scope.USER], key_type=KeyType.STRING
    )
    ScopedPreference.set("test", "user_preferences", Scope.USER, "foo", user=admin_user)

    assert (
        ScopedPreference.get("test", "user_preferences", Scope.USER, user=admin_user)
        == "foo"
    )
    assert (
        ScopedPreference.get("test", "user_preferences", Scope.USER, user=user_nobody)
        is None
    )


@pytest.mark.django_db
def test_preferences_set_get_USER_overwrite(admin_user):
    """test if a USER scoped setting returns the correct user and key"""
    PreferencesRegistry.register(
        "test", "user_preferences", [Scope.USER], key_type=KeyType.STRING
    )
    ScopedPreference.set("test", "user_preferences", Scope.USER, "foo", user=admin_user)
    ScopedPreference.set(
        "test", "user_preferences", Scope.USER, "foo2", user=admin_user
    )

    assert (
        ScopedPreference.get("test", "user_preferences", Scope.USER, user=admin_user)
        == "foo2"
    )


@pytest.mark.django_db
def test_preferences_set_get_VENDOR():
    PreferencesRegistry.register(
        "test", "vendor_preferences", [Scope.VENDOR], key_type=KeyType.STRING
    )
    ScopedPreference.set("test", "vendor_preferences", Scope.VENDOR, "shdkjhf")

    assert ScopedPreference.get("test", "vendor_preferences", Scope.VENDOR) == "shdkjhf"


@pytest.mark.django_db
def test_preferences_set_get_VENDOR_auto_keytype():
    PreferencesRegistry.register("test", "vendor_preferences", [Scope.VENDOR])
    ScopedPreference.set("test", "vendor_preferences", Scope.VENDOR, "shdkjhf")

    assert ScopedPreference.get("test", "vendor_preferences", Scope.VENDOR) == "shdkjhf"


@pytest.mark.django_db
def test_preferencesset_get_default_scope():
    PreferencesRegistry.register(
        "test", "vendor_preference2", [Scope.VENDOR], KeyType.INTEGER
    )
    ScopedPreference.set("test", "vendor_preference2", Scope.VENDOR, 42)

    result = ScopedPreference.get("test", "vendor_preference2", Scope.VENDOR)
    assert result == 42
    pass


@pytest.mark.django_db
def test_preferences_set_get_without_user_fk_object():
    PreferencesRegistry.register(
        "test", "vendor_preferences", [Scope.USER], key_type=KeyType.INTEGER
    )
    with pytest.raises(AttributeError):
        ScopedPreference.set("test", "vendor_preferences", Scope.USER, 45)


@pytest.mark.django_db
def test_preferences_set_get_specific_scope(user_admin, user_nobody, dummy_group):
    """Make sure scope with the highest priority is returned when more than one scopes
    are available for a key"""
    PreferencesRegistry.register(
        "test",
        "multiple_preferences",
        [Scope.VENDOR, Scope.USER, Scope.GROUP],
        key_type=KeyType.INTEGER,
    )
    ScopedPreference.set("test", "multiple_preferences", Scope.VENDOR, 42)
    ScopedPreference.set(
        "test", "multiple_preferences", Scope.USER, 43, user=user_admin
    )
    ScopedPreference.set(
        "test", "multiple_preferences", Scope.GROUP, 52, group=dummy_group
    )  # dummy

    assert (
        ScopedPreference.get(
            "test", "multiple_preferences", Scope.USER, user=user_admin
        )
        == 43
    )


@pytest.mark.django_db
def test_get_effective(user_a, user_nobody):
    PreferencesRegistry.register(
        "test",
        "some_preference",
        [Scope.VENDOR, Scope.TENANT, Scope.USER],
        key_type=KeyType.INTEGER,
    )

    # try to fetch users preference. if not exists, take fallback:VENDOR
    ScopedPreference.set("test", "some_preference", scope=Scope.VENDOR, value=5)
    assert (
        ScopedPreference.get_effective(
            "test", "some_preference", Scope.USER, user=user_a
        )
        == 5
    )

    # test USER preference when no USER exists. fallback MUST be TENANT
    ScopedPreference.set(
        "test", "some_preference", scope=Scope.TENANT, value=23, tenant=user_a.tenant
    )
    assert (
        ScopedPreference.get_effective(
            "test", "some_preference", Scope.USER, user=user_a
        )
        == 23
    )

    # test users own preference. No fallback
    ScopedPreference.set(
        "test", "some_preference", scope=Scope.USER, value=2, user=user_a
    )
    assert (
        ScopedPreference.get_effective(
            "test", "some_preference", Scope.USER, user=user_a
        )
        == 2
    )

    # test USER with no tenant, fallback must be VENDOR
    assert (
        ScopedPreference.get_effective(
            "test", "some_preference", Scope.USER, user=user_nobody  # nonexistent
        )
        == 5
    )
