import pytest
from django.contrib.auth import get_user_model

from medux.common.models import Tenant, SexChoices

User = get_user_model()


@pytest.fixture
def user_admin():
    # There must be an admin user in the database, by data migration.
    return User.objects.get(username="admin")


@pytest.fixture
def tenant_a():
    return Tenant.objects.create(
        first_name="Tenant", last_name="A", sex=SexChoices.MALE, address="nowhere"
    )


@pytest.fixture
def user_nobody():
    return User.objects.create_user(username="nobody")


@pytest.fixture
def user_a(tenant_a):
    return User.objects.create_user(username="a", tenant=tenant_a)
