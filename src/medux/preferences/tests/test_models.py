import pytest
from django.core.exceptions import ValidationError

from medux.preferences.definitions import Scope
from medux.preferences.models import ScopedPreference


@pytest.mark.django_db
def test_create_model():
    m = ScopedPreference.objects.create(
        namespace="foo", key="bar", scope=Scope.VENDOR, value="baz"
    )

    assert m.namespace == "foo"
    assert m.key == "bar"
    assert m.scope == Scope.VENDOR


@pytest.mark.django_db
def test_clean_lowercase():
    m = ScopedPreference.objects.create(
        namespace="Foo", key="bAr", scope=Scope.VENDOR, value="baz"
    )

    assert m.namespace == "foo"
    assert m.key == "bar"


@pytest.mark.django_db
def test_clean_no_user():
    with pytest.raises(ValidationError):
        ScopedPreference.objects.create(
            namespace="foo", key="bar", scope=Scope.USER, value="baz"
        ).clean()


@pytest.mark.django_db
def test_clean_no_group():
    with pytest.raises(ValidationError):
        ScopedPreference.objects.create(
            namespace="foo", key="bar", scope=Scope.GROUP, value="baz"
        ).clean()


# def test_clean_no_device():
#     with pytest.raises(ValidationError):
#         m = ScopedPreference(
#             namespace="foo", key="bar", scope=Scope.DEVICE, value="baz"
#         )
#         m.clean()


@pytest.mark.django_db
def test_clean_no_tenant():
    with pytest.raises(ValidationError):
        ScopedPreference.objects.create(
            namespace="foo", key="bar", scope=Scope.TENANT, value="baz"
        ).clean()
