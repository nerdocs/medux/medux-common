from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Q
from django.views.generic import ListView, FormView
from django.views.generic.edit import FormMixin

from medux.preferences.definitions import Scope
from medux.preferences.forms import PreferencesForm
from medux.preferences.models import ScopedPreference


# TODO: delete this view - deprecated
class ScopedPreferencesUpdateView(FormMixin, ListView):
    """
    This view shows a list of given preferences, and makes them editable (if possible).

    Preferences that are not editable within the given scope are displayed ad read-only.
    If the current user would have permissions to edit more than one scope,
    only the given one is displayed.
    """

    preferences: list[str] = []
    """A list of preferences that should be editable in this view, 
    e.g. "core.regular_updates"."""

    scope: Scope = Scope.USER
    """The scope this view uses for editing."""

    def get_queryset(self):
        query = Q()
        for namespace, key in [p.split(".") for p in self.preferences]:
            query &= Q(base__namespace=namespace, base__key=key, scope=self.scope)
        return ScopedPreference.objects.filter(query)


class PreferencesView(PermissionRequiredMixin, FormView):
    """This mixin adds a dynamical form to the view that allows the user to edit
    preferences.

    All views that live in the `adm` namespace (and are shown in the administration
    page) should inherit from this class.

    Attributes:
        preferences: A list of preferences in the form `namespace.key`that should be
        editable in this Form view, e.g. "core.regular_updates".
    """

    preferences: list[str] = []
    scope: Scope = Scope.TENANT
    form_class = PreferencesForm
    permission_required = "common.change_tenant"

    def get_form_kwargs(self):
        """Returns the keyword arguments for the form: the preferences and the scope."""
        kwargs = super().get_form_kwargs()
        # kwargs["preferences"] = self.preferences
        # kwargs["scope"] = self.scope
        return kwargs
