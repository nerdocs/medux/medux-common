from django import forms

from medux.preferences.definitions import KeyType
from medux.preferences.registry import PreferencesRegistry

preferences_widget_map = {
    KeyType.STRING: (forms.CharField, forms.TextInput),
    KeyType.BOOLEAN: (forms.BooleanField, forms.CheckboxInput),
    KeyType.INTEGER: (forms.IntegerField, forms.NumberInput),
    # KeyType.FLOAT: (forms.FloatField, forms.NumberInput),
    # KeyType.DATE: (forms.DateField, forms.DateInput),
    # KeyType.TIME: (forms.TimeField, forms.TimeInput),
    # KeyType.DATETIME: (forms.DateTimeField, forms.DateTimeInput),
}


class PreferencesForm(forms.Form):
    """A dynamically generated form for administration/preferences."""

    def __init__(self, **kwargs):
        self.preferences = kwargs.pop("preferences", [])
        self.scope = kwargs.pop("scope", None)
        super().__init__(**kwargs)
        for namespace, key in [p.split(".") for p in self.preferences]:
            if not PreferencesRegistry.exists(namespace, key, self.scope):
                raise AttributeError(
                    f"{self.__class__.__name__} was passed "
                    f"'{namespace}.{key} [{self.scope.name}]', "
                    f"but this preference does not exist."
                )
            field = preferences_widget_map.get(
                PreferencesRegistry.key_type(namespace, key)
            )
            self.fields[f"{namespace}.{key}"] = field
