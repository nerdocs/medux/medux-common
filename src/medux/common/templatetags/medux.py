import calendar
import decimal
from month import Month

from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django import template
from django.db.models import Model

register = template.Library()


@register.filter
def class_name(model: Model):
    """:returns the singular verbose name of the given object's class.

    This can be used in templates:

    ```django
    <div>Should this {{ object|class_name }} really be deleted?</div>
    ```
    """
    return model._meta.verbose_name


@register.filter
def class_name_plural(model: Model):
    """:returns the plural verbose name of the given object's class.

    This can be used in templates:

    ```django
    <div>Should this {{ object|class_name_plural }} really be deleted?</div>
    ```
    """
    return model._meta.verbose_name_plural


@register.filter()
def as_hours(timedelta_obj) -> decimal.Decimal:
    """Convert a datetime.timedelta object into hours"""
    if timedelta_obj:
        return decimal.Decimal(timedelta_obj.total_seconds() / 3600)
    else:
        return decimal.Decimal(0)


@register.filter
@mark_safe
def prefix_plus(value: int | float | decimal.Decimal):
    """Format a value as positive/negative.

    if positive, it gets prefixed with a "+", and gets a green text.
    if negative, it gets a red text.
    """
    if not value:
        return ""
    if isinstance(value, str):
        # Convert the string to a Decimal object
        # this doesn't cope with English thousands separators, but... %&!$§
        value = decimal.Decimal(value.replace(",", "."))
    if value > 0:
        return f"<span class='text-lime'>+{value}</span>"
    elif value < 0:
        return f"<span class='text-danger'>{value}</span>"
    else:
        return value


@register.filter
def month_name(month):
    """:returns the translated month name of an int representation (1-12)"""
    if not month:
        return ""
    if isinstance(month, Month):
        month = month.month
    return _(calendar.month_name[month])
