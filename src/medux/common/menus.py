from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from conjunto.menu import IMenuItem
from medux.preferences.definitions import ScopeIcons


def is_superuser(request):
    return request.user.is_superuser


def is_staff(request):
    return request.user.is_staff


def is_authenticated(request):
    return request.user.is_authenticated


def is_employee(request):
    return not request.user.is_anonymous and request.user.is_employee


class DjangoAdmin(IMenuItem):
    menu = "user"
    title = _("Django Admin")
    url = reverse_lazy("admin:index")
    weight = 80
    icon = "gear"
    separator = True
    check = is_staff


class Logout(IMenuItem):
    menu = "user"
    title = _("Logout")
    url = reverse_lazy("logout")
    weight = 90
    separator = True
    icon = "box-arrow-right"
    check = is_authenticated


class Profile(IMenuItem):
    menu = "user"
    title = _("Edit profile")
    url = reverse_lazy("profile")
    icon = "gear"
    check = is_employee


class Administration(IMenuItem):
    menu = "user"
    title = _("Administration")
    url = reverse_lazy("adm:system-administration")
    icon = "gear"
    required_permissions = "common.change_tenant"


class TenantAdmin(IMenuItem):
    menu = "administration"
    title = _("Tenant")
    slug = "tenant"
    url = "#"
    icon = ScopeIcons.TENANT
    required_permissions = "common.change_tenant"
    collapsed = False


class TenantMasterData(IMenuItem):
    menu = "administration"
    title = _("Master data")
    slug = "tenant__master-data"
    url = reverse_lazy("adm:tenant:master-data")
    icon = "person-fill"


class TenantLocalization(IMenuItem):
    menu = "administration"
    title = _("Localization")
    slug = "tenant__localization"
    url = reverse_lazy("adm:tenant:localization")
    icon = "translate"
    required_permissions = "common.change_tenant"


class EmployeeAdmin(IMenuItem):
    menu = "administration"
    title = _("Employees")
    slug = "tenant__employees"
    url = reverse_lazy("adm:employee:list")
    icon = "people"
    required_permissions = "common.change_tenant"
