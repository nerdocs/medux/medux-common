(() => {
  const sortables = document.querySelectorAll('.sortable');
  sortables.forEach((sortable) => {
    new Sortable(sortable, {
      animation: 150,
      ghostClass: 'bg-info',
      handle: '.handle'
    })
  })

  // -------------- Modals --------------
  // Many thanks to Benoit Blanchon: https://blog.benoitblanchon.fr/django-htmx-modal-form/
  let modal = new bootstrap.Modal(document.getElementById('modal'))  // hide the dialog at empty response.
  // This maybe additionally could check for status_core==204
  htmx.on('htmx:beforeSwap', (e) => {
    // Empty response targeting #dialog => hide the modal
    if (e.detail.target.id === 'dialog' && !e.detail.xhr.response) {
      modal.hide()
      e.detail.shouldSwap = false
    }
  })
  htmx.on('htmx:afterSwap', (e) => {
    // Response targeting #dialog => show the modal
    if (e.detail.target.id === 'dialog') {
      modal.show()
    }
  })

  // set autofocus on modal fields
  htmx.on('shown.bs.modal', (e) => {
    const modal = document.getElementById('modal')

    // first, try to find a (not hidden) element that has the "autofocus" attribute
    const autofocusEl = modal.querySelector('[autofocus]:not([type="hidden"])')
    if (autofocusEl && !autofocusEl.hidden) {
      autofocusEl.focus()
    } else {
      // if not applicable, set focus to first not-hidden input on modal (hidden = csrf_input, etc)
      const inputEls = modal.querySelectorAll('input:not([type="hidden"]), textarea:not([type="hidden"])');
      for (const inputEl of inputEls) {
        if (!inputEl.hidden && !inputEl.disabled) {
          inputEl.focus()
          return
        }
      }
      // if form contains no input fields, place focus on submit button
      const submit = modal.querySelector('button[type=submit]');
      if (submit) {
        submit.focus();
      }
    }
  });

  // empty the dialog on hide
  htmx.on('hidden.bs.modal', () => {
    document.getElementById('dialog').innerHTML = ''
  })

  // // Litepicker
  // const litepickers = document.querySelectorAll('.textinput');
  // console.log("registering ",litepickers)
  // litepickers.forEach((litepicker) => {
  //   new Litepicker({
  //     element: litepicker
  //   });
  // })

  // update all .current-time tags system-wide with - who thought that?!? - the current time!
  function updateCurrentTime() {
    document.querySelectorAll(".current-time").forEach((element) =>  {
      element.innerText = new Date().toLocaleTimeString() // [], {hour: '2-digit', minute:'2-digit'}
    })
  }
  setInterval(updateCurrentTime, 1000);

})();
