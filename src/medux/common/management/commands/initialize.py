import logging

from django.contrib.auth import get_user_model
from django.core.management import BaseCommand, call_command
from gdaps.pluginmanager import PluginManager


User = get_user_model()
logger = logging.getLogger(__file__)


class Command(BaseCommand):
    """Loads vendor preferences.toml in each app into the database"""

    help = (
        "DEV COMMAND: Initializes the application for the first time. "
        "Sets up admin account, etc."
    )

    def handle(self, *args, **options):
        admin = User.objects.filter(username="admin").first()
        assert admin
        # if admin is None:
        #     msg = "Creating Admin account: user: 'admin', password: 'admin'... "
        #     admin = User.objects.create_user(username="admin", password="admin")
        #     admin.is_staff = True
        #     admin.is_superuser = True
        #     admin.save()
        #     msg += self.style.SUCCESS("OK")
        #     self.stdout.write(msg)

        # Initialize plugins...
        for app in PluginManager.plugins():
            self.stdout.write(f"Initializing {app.label}...")
            # noinspection PyUnresolvedReferences
            if hasattr(app, "initialize") and callable(app.initialize):
                app.initialize()

        call_command("reload_permissions")
