from pathlib import Path

from django.conf import settings
from django.core.management import BaseCommand


class Command(BaseCommand):
    """Loads vendor preferences.toml in each app into the database"""

    help = "DEV COMMAND: Load (VENDOR) preferences from a file into the database."

    def handle(self, *args, **options):
        from medux.preferences.definitions import Scope
        from medux.preferences.loaders import TomlPreferencesLoader

        # for app in PluginManager.plugins():
        filename = Path(settings.MEDUX["PREFERENCES_FILE"])
        if filename.exists():
            loader = TomlPreferencesLoader(filename, scope=Scope.VENDOR)
            loader.load()

            self.stdout.write(f"Loaded preferences from {filename} into database.")
        else:
            self.stdout.write(
                f"Error: Specified preferences file '{filename}' not found."
            )
