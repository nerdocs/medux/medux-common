from .interfaces import MeduxPluginAppConfig

__all__ = [MeduxPluginAppConfig]
