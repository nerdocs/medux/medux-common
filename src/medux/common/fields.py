from django.forms import fields

from medux.common.constants import MonthChoices


class MonthNameField(fields.Select):
    """A Select field for selecting a month.

    The choices are defined in the MonthChoices class.
    """

    def __init__(self, **kwargs):
        super().__init__(choices=MonthChoices.choices, **kwargs)
