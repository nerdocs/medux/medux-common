import pytest
from datetime import datetime

from django.utils import timezone

from medux.common.urls.converters import DateConverter


converter = DateConverter()


def test_normal_date():
    assert converter.to_python("2022-10-23") == timezone.make_aware(
        datetime(year=2022, month=10, day=23)
    )


def test_impossible_date():
    with pytest.raises(ValueError):
        converter.to_python("2022-13-23"),
