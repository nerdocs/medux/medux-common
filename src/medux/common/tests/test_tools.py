import datetime

from medux.common.tools import round_up_time, round_down_time


def test_round_up_time():
    # Test rounding up to nearest 10 minutes
    assert round_up_time(datetime.time(9, 2), 10) == datetime.time(9, 10)
    assert round_up_time(datetime.time(9, 10), 10) == datetime.time(9, 10)
    assert round_up_time(datetime.time(9, 17), 10) == datetime.time(9, 20)
    assert round_up_time(datetime.time(9, 26), 10) == datetime.time(9, 30)
    assert round_up_time(datetime.time(9, 44), 10) == datetime.time(9, 50)

    # Test rounding up to nearest 15 minutes
    assert round_up_time(datetime.time(9, 2), 15) == datetime.time(9, 15)
    assert round_up_time(datetime.time(9, 15), 15) == datetime.time(9, 15)
    assert round_up_time(datetime.time(9, 17), 15) == datetime.time(9, 30)
    assert round_up_time(datetime.time(9, 31), 15) == datetime.time(9, 45)
    assert round_up_time(datetime.time(9, 44), 15) == datetime.time(9, 45)
    assert round_up_time(datetime.time(23, 44), 15) == datetime.time(23, 45)

    # Test rounding up to nearest 30 minutes
    assert round_up_time(datetime.time(9, 2), 30) == datetime.time(9, 30)
    assert round_up_time(datetime.time(9, 30), 30) == datetime.time(9, 30)
    assert round_up_time(datetime.time(9, 31), 30) == datetime.time(10, 0)

    # Test rounding up to nearest 60 minutes
    assert round_up_time(datetime.time(9, 2), 60) == datetime.time(10, 0)
    assert round_up_time(datetime.time(23, 31), 60) == datetime.time(0, 0)

    # Test rounding up at end of day
    assert round_up_time(datetime.time(23, 46), 15) == datetime.time(0, 0)
    assert round_up_time(datetime.time(23, 44), 60) == datetime.time(0, 0)


def test_round_down_time():
    # Test rounding down to nearest 10 minutes
    assert round_down_time(datetime.time(9, 2), 10) == datetime.time(9, 0)
    assert round_down_time(datetime.time(9, 17), 10) == datetime.time(9, 10)
    assert round_down_time(datetime.time(9, 26), 10) == datetime.time(9, 20)
    assert round_down_time(datetime.time(9, 44), 10) == datetime.time(9, 40)
    assert round_down_time(datetime.time(9, 15), 10) == datetime.time(9, 10)

    # Test rounding down to nearest 15 minutes
    assert round_down_time(datetime.time(9, 0), 15) == datetime.time(9, 0)
    assert round_down_time(datetime.time(9, 2), 15) == datetime.time(9, 0)
    assert round_down_time(datetime.time(9, 17), 15) == datetime.time(9, 15)
    assert round_down_time(datetime.time(9, 31), 15) == datetime.time(9, 30)
    assert round_down_time(datetime.time(9, 44), 15) == datetime.time(9, 30)
    assert round_down_time(datetime.time(9, 15), 15) == datetime.time(9, 15)

    # Test rounding down to nearest 30 minutes
    assert round_down_time(datetime.time(9, 0), 30) == datetime.time(9, 0)
    assert round_down_time(datetime.time(9, 2), 30) == datetime.time(9, 0)
    assert round_down_time(datetime.time(9, 31), 30) == datetime.time(9, 30)
    assert round_down_time(datetime.time(9, 30), 30) == datetime.time(9, 30)

    # Test rounding down to nearest 60 minutes
    assert round_down_time(datetime.time(9, 2), 60) == datetime.time(9, 0)
    assert round_down_time(datetime.time(23, 31), 60) == datetime.time(23, 0)
    assert round_down_time(datetime.time(0, 0), 60) == datetime.time(0, 0)

    # Test rounding down at end of day
    assert round_down_time(datetime.time(23, 44), 15) == datetime.time(23, 30)
    assert round_down_time(datetime.time(23, 44), 60) == datetime.time(23, 0)
