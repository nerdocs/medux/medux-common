import pytest
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from unittest import mock
from medux.common.constants import USERS_GROUP_NAME

User = get_user_model()


@pytest.mark.django_db
def test__add_user_to_users_group__gets_called():
    """Test if 'add_user_to_users_group' signal receiver is called
    after creating a user.
    """
    with mock.patch("common.signals.add_user_to_users_group", autospec=True) as handler:
        post_save.connect(
            handler, sender=User, dispatch_uid="test_cache_mocked_handler"
        )
        User.objects.create_user(username="foo", password="bar")
    assert handler.call_count == 1


@pytest.mark.django_db
def test_user_is_in_users_group_after_creating_user():
    """Tests if newly created user is in "Users" group automatically"""
    foo_user = User.objects.create_user(username="foo", password="bar")
    assert foo_user.groups.filter(
        name=USERS_GROUP_NAME
    ).exists(), "User gets not added to group 'Users' after creating"
