import locale
import logging

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, ButtonHolder, Submit

from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django_countries.fields import CountryField
from django.utils.translation import gettext_lazy as _
from medux.common.models import Tenant
from medux.common.tools import (
    get_system_locales,
    language_from_locale,
    country_name_from_code,
)

logger = logging.getLogger(__file__)


class TenantLocaleForm(forms.Form):
    language = forms.CharField(
        label=_("Language"),
        max_length=7,
        help_text=_("Default language for this tenant."),
    )
    country = CountryField().formfield(
        label=_("Country"),
        help_text=_("Used for generation of localized public holiday lists etc."),
    )

    def __init__(self, *args, **kwargs):
        system_locales = get_system_locales()
        super().__init__(*args, **kwargs)
        available_languages = []
        choices = []
        for loc in system_locales:
            lang = language_from_locale(loc)
            if lang not in available_languages:
                available_languages.append(lang)
                choices.append((lang, country_name_from_code(lang)))
        self.fields["language"].widget = forms.Select(choices=choices)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(Column("language"), Column("country")),
            ButtonHolder(Submit("save", _("Save"))),
        )

    def clean(self):
        """Tries to create a locale from language & country and set it to the system.

        Raises ValidationError if the system does not support this locale.
        """
        loc = locale.getdefaultlocale()
        language = self.cleaned_data["language"]
        country = self.cleaned_data["country"]
        test_locale = f"{language}_{country}.UTF8"  # FIXME: support more than UTF8
        # temporarily try to  set the locale
        try:
            locale.setlocale(locale.LC_TIME, test_locale)
        except locale.Error as e:
            logger.error(f"Error processing {self.__class__.__name__} data: {e}")
            raise ValidationError(
                _(
                    "The locale '{test_locale}' is not supported by this "
                    "system. Please ask your administrator to install it."
                ).format(test_locale=test_locale)
            )
        locale.setlocale(locale.LC_TIME, loc)


class TenantAdministrationForm(ModelForm):
    class Meta:
        model = Tenant
        fields = "__all__"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(Column("title"), Column("sex")),
            Row(Column("first_name"), Column("last_name")),
            Row(Column("address")),
            Row(Column("phone"), Column("email")),
            "picture",
            "uuid",
            ButtonHolder(Submit("save", _("Save"))),
        )
        self.fields["uuid"].disabled = True
