import calendar
from datetime import time, datetime
from io import BytesIO
from logging import getLogger
from pathlib import Path

from django.core.files import File
from PIL import Image

logger = getLogger(__file__)

image_types = {
    "jpg": "JPEG",
    "jpeg": "JPEG",
    "png": "PNG",
    "gif": "GIF",
    "tif": "TIFF",
    "tiff": "TIFF",
}


def resize_image(image, width, height):
    """Resizes an image to a given width/height"""
    # thanks to https://stackoverflow.com/a/71919278/818131

    img = Image.open(image)
    if img.width > width or img.height > height:
        output_size = (width, height)
        img.thumbnail(output_size)
        img_filename = Path(image.file.name).name
        # get file extension
        img_suffix = Path(image.file.name).name.split(".")[-1]
        # Use file extension to determine  file type from the image_type's dictionary
        img_format = image_types[img_suffix]
        # Save resized image into the buffer with the correct file type
        buffer = BytesIO()
        img.save(buffer, format=img_format)
        # Wrap buffer in a File object
        file_object = File(buffer)
        # Save new resized file
        image.save(img_filename, file_object)


# ------------------ Date/Time functions ------------------
def round_down_time(time: time, unit: int):
    """Round down to the nearest time unit"""
    minute = (time.minute // unit) * unit
    return time.replace(minute=minute, second=0, microsecond=0)


def round_up_time(time: time, unit: int):
    """Round up to the nearest time unit, preserving "even" times.

    When getting to 24:00, return to 0:00
    """
    minute = time.minute
    if minute % unit != 0:
        minute = (minute // unit) * unit + unit
        if minute >= 60:
            minute -= 60
            hour = (time.hour + 1) % 24
        else:
            hour = time.hour
    else:
        hour = time.hour

    rounded_time = time.replace(hour=hour, minute=minute, second=0, microsecond=0)
    return rounded_time


def monthdelta(date: datetime, delta: int) -> datetime:
    # modified after https://stackoverflow.com/questions/3424899/return-datetime-object-of-previous-month
    m, y = (date.month + delta) % 12, date.year + ((date.month) + delta - 1) // 12
    if not m:
        m = 12
    d = min(date.day, calendar.monthrange(y, m)[1])

    return date.replace(year=y, month=m, day=d)
