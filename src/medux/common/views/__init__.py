import logging
from typing import Any

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.views import LoginView as DjangoLoginView
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import translation
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    TemplateView,
    UpdateView,
    FormView,
    ListView,
)
from django_tables2 import SingleTableMixin

from .multiple import MultipleFormsView, MultipleFormsMixin
from ..api.interfaces import ILoginViewExtension
from ..auth.forms import AuthenticationForm
from ..forms.tenant import TenantAdministrationForm, TenantLocaleForm
from conjunto.views import AnonymousRequiredMixin
from ..menus import is_employee
from ..models import Tenant
from ...preferences.definitions import Scope
from medux.preferences.models import ScopedPreference
from medux.preferences.tools import get_effective_preference

__all__ = [MultipleFormsView, MultipleFormsMixin]

logger = logging.getLogger(__file__)
User = get_user_model()


class LoginView(AnonymousRequiredMixin, DjangoLoginView):
    """Extendable login view with welcome message"""

    form_class = AuthenticationForm
    template_name = "common/login.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        for plugin in ILoginViewExtension:
            context.update(plugin.alter_context_data(**kwargs))

        context["welcome_message"] = _("Medically welcome to {project_title}").format(
            project_title=settings.PROJECT_TITLE
        )

        return context

    def post(self, request, *args, **kwargs):
        """Calls view extensions and then proceeds with parent's post()"""
        response = super().post(request, *args, **kwargs)
        for plugin in ILoginViewExtension:
            plugin.alter_response(request, response, *args, **kwargs)
        return response

    def form_valid(self, form):
        response = super().form_valid(form)
        for plugin in ILoginViewExtension:
            plugin.form_valid(form, response)
            # set language from tenant setting
            language = get_effective_preference("common", "language", self.request)
            if language:
                logger.debug(
                    f"Set user's ({self.request.user}) language to tenant's: "
                    f"'{language.value}'"
                )
                translation.activate(language.value)
                response.set_cookie(settings.LANGUAGE_COOKIE_NAME, language.value)

        return response


class FormActionsMixin:
    """Mixin to add to UpdateViews that enables additional action buttons.

    Each action is mapped to the corresponding method in the view, so a
    button named "delete" calls the ``delete()`` method.
    Actions available:
    * delete
    """

    # TODO: add wildcard buttons - is this a security risk?
    def post(self, request, *args, **kwargs):
        if request.POST.get("delete", ""):
            return self.delete(request, *args, **kwargs)
        else:
            return super().post(request, *args, **kwargs)


class DashboardMixin:
    """Add common needed dashboard attributes to the context.

    All views that live in the dashboard must inherit from this mixin.
    """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"tenants": Tenant.objects.all().order_by("last_name")})
        return context


class AutoFocusMixin:
    """Put the 'autofocus' attribute to a given field of a FormView.

    Set the autofocus attribute to a field of the form you are working
    with, and it will get the focus after loading the page.
    """

    # FIXME: move AutoFocusMixin to medux.common

    autofocus_field_name = None

    def get_form(self, form_class=None):
        """Return an instance of the form to be used in this view."""
        form = super().get_form(form_class)

        if self.autofocus_field_name in form.fields:
            form.fields[self.autofocus_field_name].widget.attrs.update(
                {"autofocus": True}
            )
        return form


# class FormFieldCheckView(FormView):
#     form_class = None
#     field = ""
#
#     def get_form_kwargs(self):
#         kwargs = super().get_form_kwargs()
#         kwargs.update({"request": self.request})
#         return kwargs
#
#     def post(self, request, **kwargs):
#         form = self.form_class(self.get_form_kwargs())
#         return HttpResponse(as_crispy_field(form[self.field]))
#
#
# class CheckEmailView(FormFieldCheckView):
#     form_class = UserProfileMasterDataForm
#     field = "email"
#
#
# def check_email(request):
#     """Small view that checks the email field for correctness"""
#     if not request.htmx:
#         raise PermissionDenied("Method is only allowed over HTMX.")
#     form = UserProfileMasterDataForm(request, request.GET, instance=request.user)
#     return HttpResponse(as_crispy_field(form["email"]))


class ManifestView(TemplateView):
    template_name = "common/manifest.json"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "project_title": settings.PROJECT_TITLE,
                "description": settings.PROJECT_DESCRIPTION,
                "theme_color": "#9fc015",
                "background_color": "#ffffff",
                "start_url": "/",
            }
        )
        return context


class AdministrationPageMixin(PermissionRequiredMixin):
    """A generic view mixin for pages in the `Administration` namespace"""

    # as standard, use administration.html. Override if needed.
    template_name = "common/administration.html"

    # A title to be displayed in the administration page
    title = None

    # Always redirect to same page.
    success_url = "."

    def get_title(self):
        if self.title:
            return self.title
        else:
            return _("Administration")


class AdministrationTableListView(AdministrationPageMixin, SingleTableMixin, ListView):
    template_name = "common/administration_table.html"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "add_title": _("Add {model_name}").format(
                    model_name=self.model._meta.verbose_name
                ),
                "object_add_url": reverse(
                    f"{self.model._meta.app_label}:{self.model._meta.model_name}:add"
                ),
            }
        )
        return context


class SystemAdministrationView(AdministrationPageMixin, TemplateView):
    permission_required = "common.change_tenant"  # TODO find a better permission
    title = _("System administration")


class TenantAdministrationView(AdministrationPageMixin, UpdateView):
    model = Tenant
    permission_required = "common.change_tenant"
    title = _("Tenant master data")
    form_class = TenantAdministrationForm

    def get_object(self, queryset=None):
        """Always return current authenticated user's tenant"""
        return self.request.user.tenant


class TenantLocalizationView(AdministrationPageMixin, FormView):
    permission_required = "common.change_tenant"
    form_class = TenantLocaleForm
    title = _("Tenant localization")

    def get_initial(self):
        initial = self.initial.copy()

        # try to get language and country
        cur_language = translation.get_language_from_request(self.request)
        initial["language"] = cur_language

        if is_employee(self.request.user):
            # if current user is employee, use its tenant as initial value
            country_preference: str = ScopedPreference.get(
                namespace="common",
                key="country",
                scope=Scope.TENANT,
                tenant=self.request.user.tenant,
            )
            if country_preference:
                country_code = country_preference.upper()
            else:
                logger.debug(
                    "No country available for tenant '{tenant}'. Using VENDOR preference "
                    "for country.".format(tenant=self.request.user.tenant)
                )
                # try to get country from LANGUAGE_CODE
                lc = settings.LANGUAGE_CODE.split("-")
                if len(lc) == 2:
                    country_code = lc[1].upper()
                else:
                    country_code = None

        else:
            # if user is admin etc., use vendor setting
            logger.debug(
                "No tenant available for user '{user}'. Using VENDOR preference for "
                "country.".format(user=self.request.user)
            )
            country_code = ScopedPreference.get("common", "country", Scope.VENDOR)

        initial["country"] = country_code
        return initial

    def form_valid(self, form):
        response = HttpResponseRedirect(self.get_success_url())
        language = form.cleaned_data["language"]
        if language:
            translation.activate(language)
            response.set_cookie(settings.LANGUAGE_COOKIE_NAME, language)
            ScopedPreference.set(
                namespace="common",
                key="language",
                scope=Scope.TENANT,
                tenant=self.request.user.tenant,
                value=language,
            )
        country = form.cleaned_data["country"]
        if country:
            ScopedPreference.set(
                namespace="common",
                key="country",
                scope=Scope.TENANT,
                tenant=self.request.user.tenant,
                value=country,
            )
        # TODO reload whole page (using HTMX) - to see the effects of language change.
        return response
