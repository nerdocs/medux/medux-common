from django.contrib.auth.mixins import PermissionRequiredMixin


class TenantPermissionRequiredMixin(PermissionRequiredMixin):
    """Verify for SingleObjectViews that the user has required permissions, and
    the object's tenant matches the user's tenant.
    """

    def has_permission(self):
        user = self.request.user
        obj = self.get_object()
        return user.tenant == obj.tenant and user.has_perms(
            self.get_permission_required()
        )


class TenantFilterMixin:
    """View mixin that filters objects by current user's tenant."""

    def get_queryset(self):
        return super().get_queryset().filter(tenant=self.request.user.tenant)
