from django.contrib.auth.views import LogoutView
from django.urls import path, include

import conjunto.views
from medux.common import views
from medux.common.api.interfaces import IAdministrationURL
from medux.common.api.interfaces import IWebsocketURL

# namespaced URLs
app_name = "common"

# URLs namespaced  under common/
urlpatterns = [
    # path("", views.IndexView.as_view(), name="index"),
]


class TenantAdministrationURLs(IAdministrationURL):
    namespace = "tenant"
    urlpatterns = [
        path(
            "master-data/", views.TenantAdministrationView.as_view(), name="master-data"
        ),
        path(
            "localization/",
            views.TenantLocalizationView.as_view(),
            name="localization",
        ),
    ]


# global URLs
root_urlpatterns = [
    path("manifest.json", views.ManifestView.as_view(), name="manifest"),
    path(
        "accounts/login/",
        views.LoginView.as_view(),
        name="login",
    ),
    path(
        "accounts/logout/",
        LogoutView.as_view(template_name="registration/logout.html"),
        name="logout",
    ),
    path(
        "profile/", conjunto.views.SettingsView.as_view(), name="profile"
    ),  # FIXME: rename to settings, or make Profile View!
    # path(
    #     "profile/",
    #     include((views.ProfileView.component_urls(), "common"), namespace="profile"),
    # ),
    # path("check-email/", views.CheckEmailView.as_view(), name="check-email"),
]

# system-wide administration URL patterns
adm_urlpatterns = [
    path(
        "system/",
        views.SystemAdministrationView.as_view(),
        name="system-administration",
    ),
]
# collect all administration URLs into their namespaces under "adm"
for plugin in IAdministrationURL:
    adm_urlpatterns += [
        path(
            f"{plugin.namespace}/",
            include((plugin.urlpatterns, plugin.namespace), namespace=plugin.namespace),
        )
    ]

# append admin urlpatterns to the root urlpatterns
root_urlpatterns += [
    path("administration/", include((adm_urlpatterns, "adm"), namespace="adm")),
]


websocket_urlpatterns = []

for plugin in IWebsocketURL:
    websocket_urlpatterns += plugin.urlpatterns
