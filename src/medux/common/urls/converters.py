from datetime import datetime

from django.urls import register_converter
from django.utils import timezone


class DateConverter:
    """URL converter that converts "YYYY-MM-DD" into a datetime object."""

    # thanks to https://stackoverflow.com/a/61134265/818131

    regex = r"\d{4}-\d{2}-\d{2}"

    def to_python(self, value):
        return (
            timezone.make_aware(datetime.strptime(value, "%Y-%m-%d")) if value else None
        )

    def to_url(self, value):
        return value


class TimeConverter:
    """URL converter that converts "HH-MM" or "HH:MM" into a time object."""

    regex = r"\d{2}-\d{2}"

    def to_python(self, value):
        return (
            timezone.make_aware(datetime.strptime(value.strip(), "%H-%M"))
            if value
            else None
        )

    def to_url(self, value):
        return value


register_converter(DateConverter, "date")
register_converter(TimeConverter, "time")
