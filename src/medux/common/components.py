from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.translation import gettext_lazy as _
from django.views.generic import UpdateView

from conjunto.api.interfaces import ISettingsSection
from medux.common.forms.user import UserProfileMasterDataForm, UserProfilePasswordForm

# FIXME: move that classes out of "components" namespace,
#   as that is used by django-web-components


class ProfileMasterDataSection(
    ISettingsSection, PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    name = "account"
    group = "user_profile"
    title = _("Account")
    icon = "person"
    weight = 0
    enforce_htmx = False
    form_kwargs_request = True
    form_class = UserProfileMasterDataForm
    success_message = _("Profile data saved.")


class ProfilePasswordSection(
    ISettingsSection, PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    name = "password"
    group = "user_profile"
    title = _("Password")
    icon = "key"
    weight = 0
    form_kwargs_request = True
    form_class = UserProfilePasswordForm
    success_message = _("Password saved.")
