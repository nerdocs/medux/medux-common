"""
MedUX Common testing settings.
"""
from medux.tests.settings import *  # noqa

# use a name suitable for medux common.
DATABASES["default"]["NAME"] = "medux-common-test.db"  # noqa
