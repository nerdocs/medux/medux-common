# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.7] - 2023-01-31
- add locale, static, templates in package

## [0.0.6] - 2023-01-31
- use src project layout

## [0.0.5.1] - 2023-01-30
- minor bug fix

## [0.0.5] - 2023-01-30
- Initial commit, copied files from medux