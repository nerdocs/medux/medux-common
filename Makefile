all: init locale staticfiles
production: localecompile staticfiles
LANGUAGES:=`find src/medux/common/locale/ -mindepth 1 -maxdepth 1 -type d -printf "--locale %f "`
MANAGE:="django-admin"

localecompile:
	$(MANAGE) compilemessages

localegen:
    # don't --keep-pot
	$(MANAGE) makemessages --ignore "build/*" $(LANGUAGES)


locale: localegen localecompile

test:
	pytest

coverage:
	coverage run -m pytest