# MedUX Common Plugin

Common MedUX/MedUX Online features

## General

MedUX and MedUX Online have some common functionality. This module encapsulates everything
they both need.

#### Common PluginConfig

The app configuration is declared in the module's `apps.py`. Set the plugin's metadata there in the PluginMeta class.


## Install

Nothing to do explicitly: MedUX and MedUX Online both depend on this package, so it should be installed automatically.

## License

This plugin is licensed under the [GNU Affero General Public License v3 or later (AGPLv3+)](https://www.gnu.org/licenses/agpl-3.0.txt)

